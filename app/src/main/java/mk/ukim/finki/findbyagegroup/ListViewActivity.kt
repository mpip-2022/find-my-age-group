package mk.ukim.finki.findbyagegroup

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.RecyclerView
import mk.ukim.finki.findbyagegroup.adapters.ExampleViewAdapter

class ListViewActivity : AppCompatActivity() {

    private lateinit var listView:RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_view)

        listView = findViewById(R.id.listView)

        listView.adapter = ExampleViewAdapter(loadData())
    }

    private fun loadData():MutableList<String> {
        return mutableListOf("Item 1", "Item 2", "Item 3","Item 4")
    }
}
package mk.ukim.finki.findbyagegroup.fragments

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.commit
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import mk.ukim.finki.findbyagegroup.R
import mk.ukim.finki.findbyagegroup.databinding.FragmentFirstBinding
import mk.ukim.finki.findbyagegroup.viewmodels.NicknameViewModel

class FirstFragment : Fragment(R.layout.fragment_first) {

    private var _binding: FragmentFirstBinding? = null
    private val binding get() = _binding!!
    private val nicknameViewModel:NicknameViewModel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentFirstBinding.bind(view)

        binding.btnGoToSecondFragment.setOnClickListener {
            nicknameViewModel.selectNickname(binding.editNickname.text.toString())
            parentFragmentManager.commit {
                replace(R.id.fragment_container_view,SecondFragment())
                setReorderingAllowed(true)
                addToBackStack(null)
            }
        }

        nicknameViewModel.nickname.observe(viewLifecycleOwner) {
            binding.textView.text = it
        }
    }
}
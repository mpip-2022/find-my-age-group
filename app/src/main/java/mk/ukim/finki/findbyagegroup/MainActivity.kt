package mk.ukim.finki.findbyagegroup

import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.CalendarContract
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.ViewModelProvider
import mk.ukim.finki.findbyagegroup.extensions.toInt
import mk.ukim.finki.findbyagegroup.viewmodels.AgeViewModel
import java.util.*

class MainActivity : AppCompatActivity() {

    private lateinit var txtAgeGroup: TextView
    private lateinit var editTextAge: EditText
    private lateinit var btnSubmit: Button
    private lateinit var btnGoToExplicitActivity: Button
    private lateinit var btnGoToImplicitActivity: Button
    private lateinit var btnGoToIntentActivity:Button
    private lateinit var btnGoToListView:Button
    private lateinit var btnGoToStudentsActivity: Button

    private lateinit var ageViewModel: AgeViewModel

    var resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == Activity.RESULT_OK) {
            val data: Intent? = result.data
            txtAgeGroup.text = data?.getStringExtra("ageGroup")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        txtAgeGroup = findViewById(R.id.txtAgeGroup)
        editTextAge = findViewById(R.id.editTextAge)
        btnSubmit = findViewById(R.id.btnSubmit)
        btnGoToExplicitActivity = findViewById<Button>(R.id.btnGoToExplicitActivity)
        btnGoToImplicitActivity = findViewById(R.id.btnGoToImplicitActivity)
        btnGoToIntentActivity = findViewById(R.id.btnGoToIntentActivity)
        btnGoToListView = findViewById(R.id.btnGoToListView)
        btnGoToStudentsActivity = findViewById(R.id.btnGoToStudentsActivity)


        ageViewModel = ViewModelProvider(this)[AgeViewModel::class.java]

        editTextAge.setText(ageViewModel.getAgeValue().toString())

        btnSubmit.setOnClickListener {
            ageViewModel.setAgeValue(editTextAge.text.toInt())
        }

        btnGoToExplicitActivity.setOnClickListener {
            Intent(this, ExplicitActivity::class.java).let { i ->
                i.putExtra("ageGroup", editTextAge.text.toString())
                startActivity(i)
            }
        }

        btnGoToListView.setOnClickListener {
            startActivity(Intent(this,ListViewActivity::class.java))
        }

        btnGoToStudentsActivity.setOnClickListener {
            startActivity(Intent(this,StudentsListActivity::class.java))
        }

        btnGoToImplicitActivity.setOnClickListener {
//            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://maps.google.com/?q=Rugjer Boskovikj Skopje")))

//            val emailIntent = Intent(Intent.ACTION_SEND).let { emailIntent ->
//                emailIntent.type = "text/plain"
//                emailIntent.putExtra(Intent.EXTRA_EMAIL, arrayOf("jon@example.com")) // recipients
//
//                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Email subject")
//                emailIntent.putExtra(Intent.EXTRA_TEXT, "Email message text")
//                startActivity(emailIntent)
//            }

//            Intent(Intent.ACTION_INSERT, CalendarContract.Events.CONTENT_URI).let { calendarIntent ->
//                val beginTime: Calendar = Calendar.getInstance()
//                beginTime.set(2012, 0, 19, 7, 30)
//                val endTime: Calendar = Calendar.getInstance()
//                endTime.set(2012, 0, 19, 10, 30)
//                calendarIntent.putExtra(
//                    CalendarContract.EXTRA_EVENT_BEGIN_TIME,
//                    beginTime.getTimeInMillis()
//                )
//                calendarIntent.putExtra(
//                    CalendarContract.EXTRA_EVENT_END_TIME,
//                    endTime.getTimeInMillis()
//                )
//                calendarIntent.putExtra(CalendarContract.Events.TITLE, "Ninja class")
//                calendarIntent.putExtra(CalendarContract.Events.EVENT_LOCATION, "Secret dojo")
//                startActivity(calendarIntent)
//            }

            val webpage: Uri = Uri.parse("http://www.android.com")
            val webIntent = Intent(Intent.ACTION_VIEW, webpage)
            startActivity(webIntent)
        }

        btnGoToIntentActivity.setOnClickListener{ _ ->
            Intent().apply {
                action = "mk.ukim.finki.age"
                type = "text/plain"
            }.let { intent ->
                intent.putExtra("ageValue",editTextAge.text.toString())
                //startActivity(Intent.createChooser(intent,"Choose the app for your intent"))
                resultLauncher.launch(intent)
            }
        }

        editTextAge.addTextChangedListener { newText ->
            ageViewModel.setAgeValue(newText.toInt())
        }

        ageViewModel.getAge().observe(this) {
            txtAgeGroup.text = ageViewModel.calcAgeGroup()
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("MainActivity", "onDestroy")
    }

}
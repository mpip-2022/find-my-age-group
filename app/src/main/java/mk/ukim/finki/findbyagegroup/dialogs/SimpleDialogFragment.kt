package mk.ukim.finki.findbyagegroup.dialogs

import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import mk.ukim.finki.findbyagegroup.R

class SimpleDialogFragment:DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            builder.setMessage(R.string.simple_dialog_title)
                .setPositiveButton(R.string.ok, DialogInterface.OnClickListener { dialog, id -> })

            builder.create()
        }?:throw IllegalStateException("Activity can not be null")
    }
}